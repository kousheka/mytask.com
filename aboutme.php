<?php
require_once('inc/config.php');
require_once('inc/security.php');
?>
<!doctype html>
<html class="no-js" lang="fr">
  <head>
		<?php require_once('template/head.php'); ?>
  </head>
  <body>
		<div class="off-canvas-wrapper">
			<?php require_once('template/header.php');	?>

			<main class="container off-canvas-content" data-off-canvas-content>
				<div class="row">
					<h1 class="page-title">About Me</h1>
					
         <div id="page">
            <p> 
            <img src="assets/img/<?php echo $_SESSION['userid']; ?>.jpg" width=150 align="right" />
               
               <h3><strong>Kousheka BOOPATHI </strong> </h3>
                Rte des Vieux-Chênes 56<br>
                1700 Fribourg <br>
                079 000 00 00 <br>
                10.07.1996<br>
                <a href="mailto:bkoushe@hotmail.com">bkoushe@hotmail.com</a>
                <br>
            </p>
            <p>
               <h3><strong>Formation</strong> </h3>
               <table>
                  <tr>
                     <td width="100px" align="center"><strong>Actuellement</strong></td><td align="justify">Bachelor d’ingénieur en Télécommunication – HEIA Fribourg</td>
                  </tr>
                  <tr>
                     <td width="100px" align="center"><strong>2012 - 2016</strong></td><td align="justify">Apprentissage en Informatique –  SICPA SA CFC & MPT</td>
                  </tr>
                   <tr>
                     <td width="100px" align="center"><strong>2002 -2006 <br> & <br>2009 - 2012</strong></td><td align="justify">Scolarisation obligatoire – En suisse</td>
                  </tr>
                   <tr>
                     <td width="100px" align="center"><strong>2006 - 2009</strong></td><td align="justify">Scolarisation obligatoire – En Inde</td>
                  </tr>
               </table>
               </p>
               <br>

               <p>
                  <h4><strong>Apprentissage</strong> </h4>
                  <br>
                  <table>
                  <tr>
                     <td width="100px" align="center"><strong>2012 - 2016 </strong></td><td align="justify">SICPA SA – Apprentissage d’informaticienne 
                     <br>
                        <ul>
                           <li>Stage de minimum 3 mois dans tous les départements d’informatique</li>
                           <li>Réseau, Linux, SAP, Programmation, Infrastructure, Help desk et technicien de 2eme niveau</li>
                           <li>TPI : Application web de base de connaissance (Drupal, server LAMP, script de backup avec Bash)</li>
                        </ul>
                     </td>
                  </tr>
                  </table>
               </p>
               <br>
               <p>
                  <h4><strong>Langues</strong> </h4>
                  <table>
                  <tr>
                     <td width="100px" align="center"><strong>Francais</strong></td><td>Très bonne connaissance.</td>
                  </tr>
                     <tr>
                     <td width="100px" align="center"><strong>Anglais</strong></td><td>Très bonne connaissance.Niveau C1</td>
                  </tr>
                     <tr>
                     <td width="100px" align="center"><strong>Allemand</strong></td><td>Connaissance scolaire. Deux semaines linguistiques en Allemagne. </td>
                  </tr>
                  </table>
               </p>
               <br>
               <p>
                  <h4><strong>COMPETENCE PERSONNELLES</strong> </h4>
                  Polie, ponctuelle, innovante, flexible, volontaire et autonome.
               </p>
               <br>
               <p>
                  <h4><strong>CENTRES D'INTERET</strong> </h4>
                  <table>
                  <tr>
                     <td width="100px" align="center"><strong>Hobby</strong></td><td>Lecture de roman</td>
                  </tr>
                     <tr>
                     <td width="100px" align="center"><strong>Sport</strong></td><td>Badminton, Tennis et Hiphop</td>
                  </tr>
                  </table>
               </p>

   		</div>	

			</main>

			<?php require_once('template/footer.php'); ?>
		</div>
  </body>
</html>