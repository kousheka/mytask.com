<?php
require_once('inc/config.php');

if(isset($_SESSION['userid']))
	header('Location:index.php');
?>
<!doctype html>
<html class="no-js" lang="fr">
  <head>
		<?php require_once('template/head.php'); ?>
  </head>
  <body>
			<main class="row columns small-12 large-4">
					<h1 class="page-title">Login</h1>
					<form method="post" action="logme.php">
						<label for="email">E-mail</label>
						<input type="text" name="email" id="email"/>

						<label for="password">Password</label>
						<input type="password" name="password" id="password"/>

						<input type="submit" value="Se connecter" class="button"/>
					</form>
			</main>

			<?php require_once('inc/script.php'); ?>
  </body>
</html>
