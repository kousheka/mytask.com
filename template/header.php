<?php
require_once('inc/config.php');
require_once('inc/security.php');
?>


<header class="top-row-expanded">
	<div class="top-bar">
		<div class="top-bar-title">
			<button class="menu-icon hide-for-large" type="button" data-toggle="offCanvas"></button>
			<a href="index.php">Task Manager</a>
		</div>
		<div class="top-bar-right">
			<?php
			$query =	$db -> prepare('SELECT * FROM user WHERE id = ?');
			$query -> execute(array($_SESSION['userid']));
			$data = $query -> fetch();
			?>
			<ul class="dropdown menu" data-dropdown-menu>
				<li>
					<img src="assets/img/<?php echo $_SESSION['userid']; ?>.jpg" class="header-user-img" />
					<ul class="menu">
						<li><a href="edituser.php?id=<?php echo $_SESSION['userid']; ?>"><?php echo $data['name']; ?></a></li>
						<li><a href="logout.php">Logout</a></li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
</header>


<nav class="nav off-canvas-absolute position-left reveal-for-large" id="offCanvas" data-off-canvas>
<div class="side-bar">
	<ul class="vertical menu">
		<li><a href="add.php">Add task</a></li>
		<li><a href="users.php">User list</a></li>
		<li><a href="adduser.php">Add user</a></li>
		<li><a href="aboutme.php">About Me</a></li>
		<li><a href="aboutproject.php">About Project</a></li>
	</ul>
	</div>
</nav>
