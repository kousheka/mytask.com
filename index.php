<?php
require_once('inc/config.php');
require_once('inc/security.php');
?>
<!doctype html>
<html class="no-js" lang="fr">
  <head>
		<?php require_once('template/head.php'); ?>
  </head>
  <body>
		<div class="off-canvas-wrapper">
			<?php require_once('template/header.php'); ?>

			<main class="container off-canvas-content" data-off-canvas-content>
				<div class="row">
					<h1 class="page-title">Tasks</h1>
					<ul class="tasklist">
						<li class="tasklist-header">
							<span class="tasklist-item-id">
								ID
							</span>
							<span class="tasklist-item-priority">
								Priority
							</span>
							<span class="tasklist-item-description">
								Description
							</span>
							<span class="tasklist-item-creator">
								Created by
							</span>
							<span class="tasklist-item-assignee">
								Assigned by
							</span>
							<span class="tasklist-item-due">
								Due date
							</span>
							<span class="tasklist-item-actions">
								Actions
							</span>
						</li>
						<?php
	          $query = $db -> prepare('SELECT
									task.id,
									description,
									created_at,
									due_at,
									priority,
									status,
									creator.id as creator_id,
									creator.name as creator_name,
									assignee.id as assignee_id,
									assignee.name as assignee_name
									FROM task
									INNER JOIN user as creator on created_by = creator.id
									LEFT JOIN user as finishor on done_by = finishor.id
									INNER JOIN user as assignee on assigned_to = assignee.id');

						$query -> execute();
	          while($data = $query -> fetch()):
	          ?>
						<li class="tasklist-item<?php if($data['status'] == 'close'): ?> tasklist-item-close<?php endif; ?>">
	            <span class="tasklist-item-id">
	              <?php echo $data['id']; ?>
	            </span>
							<span class="tasklist-item-priority">
	              <?php echo $data['priority']; ?>
	            </span>
	            <span class="tasklist-item-description">
	              <?php echo $data['description']; ?>
	            </span>
							<span class="tasklist-item-creator">
								<?php echo $data['creator_name']; ?>
							</span>
							<span class="tasklist-item-assignee">
								<?php echo $data['assignee_name']; ?>
							</span>
	            <span class="tasklist-item-due">
	              <?php echo $data['due_at']; ?>
	            </span>
	            <span class="tasklist-item-actions">
								<a href="edit.php?id=<?php echo $data['id']; ?>">
	                <i class="fa fa-pencil" aria-hidden="true"></i>
	              </a>
	              <a href="#" data-delete="<?php echo $data['id']; ?>">
	                <i class="fa fa-times" aria-hidden="true"></i>
	              </a>
								<a href="#" data-done="<?php echo $data['id']; ?>">
	                <i class="fa fa-check" aria-hidden="true"></i>
	              </a>
	            </span>
	          </li>
	          <?php endwhile; ?>
					</ul>
				</div>
			</main>

			<?php require('template/footer.php'); ?>
		</div>
  </body>
</html>
