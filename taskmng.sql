-- Adminer 4.3.0 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

INSERT INTO `task` (`id`, `description`, `created_at`, `created_by`, `due_at`, `assigned_to`, `priority`, `status`, `done_by`) VALUES
(1,	'Test',	'2017-06-07 00:00:00',	1,	'2017-06-30 00:00:00',	1,	1,	'open',	1),
(2,	'Create project',	'2017-06-21 15:33:26',	2,	'2017-11-10 00:00:00',	3,	1,	'open',	NULL);

INSERT INTO `user` (`id`, `name`, `email`, `password`) VALUES
(1,	'root',	'root@taskmng.ch',	'root'),
(2,	'Kousheka',	'Kousheka@taskmng.ch',	'root'),
(3,	'user1',	'user1@taskmng.ch',	'root'),
(4,	'user2',	'user2@taskmng.ch',	'root');

-- 2017-06-25 21:45:19
