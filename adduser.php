<?php
require_once('inc/config.php');
require_once('inc/security.php');
?>
<!doctype html>
<html class="no-js" lang="fr">
  <head>
		<?php require_once('template/head.php'); ?>
  </head>
  <body>
		<div class="off-canvas-wrapper">
			<?php require_once('template/header.php'); ?>

			<main class="container off-canvas-content" data-off-canvas-content>
				<div class="row">
					<h1 class="page-title">Add User</h1>
					<form method="post" action="adduser-action.php" class="small-12 medium-6 collumn">
            <label>Name</label>
            <input type="text" name="name"/>
						<label>E-mail</label>
            <input type="text" name="email"/>
						<label>Password</label>
            <input type="password" name="password"/>
            <input type="submit" value="Add" class="button"/>
	        </form>
				</div>
			</main>

			<?php require_once('template/footer.php'); ?>
		</div>
  </body>
</html>
